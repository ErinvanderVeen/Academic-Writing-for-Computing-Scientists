\section{Creating Machines}
\subsection{Non-Determinism}
In the previous section I hinted on the idea of a program being in multiple ``location states''. This happens when a state has two or more transitions for which the conditions are true. Consider, for example, the following machine that prints whether a number is divisble by 2 and 3.

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm, semithick]
    \node[initial, state] (a) {$q_a$};
    \node[state] (b) [right = 8cm of a] {$q_b$};
    
    \draw (a) edge[bend left] node{\begin{tabular}{c}
                                        $2 | x$ \\ \hline
                                        \lstinline[language=C++]{std::cout << "2 divides x!";}
                                    \end{tabular}} (b);
    \draw (a) edge[bend right] node[below]{\begin{tabular}{c}
                                        $3 | x$ \\ \hline
                                        \lstinline[language=C++]{std::cout << "3 divides x!";}
                                    \end{tabular}} (b);
\end{tikzpicture}

This kind of non-determinism is fine. But we run into problems when two ``threads'' concurrently assign a different value to the same variable. Such problems can be solved by using semaphores. Consider the following machines where two threads are spawned. One then assigns 1 to x, the other assigns 2 to x. The final value of x is undefined.

\begin{tikzpicture}
    \node[initial, state] (a) {$q_a$};
    \node[state] (b) [right = 8cm of a] {$q_b$};
    
    \draw (a) edge[bend left] node[above]{\lstinline[language=C++]{x = 1;}} (b);
    \draw (a) edge[bend right] node[below]{\lstinline[language=C++]{x = 2;}} (b);
\end{tikzpicture}

Suppose we add some boolean $a$ to the variable state, and initiate it with ``false''. $a$ will be ``true'' if x is currently being edited. It will be ``false'' if it is not. This means, however, that the it must also be stated that a transition is considered in a single flow. It cannot happen that the statement of a transition is executed if the previous action was not checking the condition of said transition.

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm, semithick]
    \node[initial, state] (a) {$q_a$};
    \node[state] (b) [right = 4cm of a] {$q_b$};
    
    \draw (a) edge[bend left] node{\begin{tabular}{c}
                                        $!a$ \\ \hline
                                        \lstinline[language=C++]{x = 1;} \\
                                    \end{tabular}} (b);
    \draw (a) edge[bend right] node{\begin{tabular}{c}
                                        $!a$ \\ \hline
                                        \lstinline[language=C++]{x = 2;} \\
                                    \end{tabular}} (b);
\end{tikzpicture}

If, for some reason, both statement still need to be executed, one can loop the other ``thread'' untill it's condition is met. Here is one attemt.

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm, semithick]
    \node[initial, state] (a) {$q_a$};
    \node[state] (b) [right = 3cm of a] {$q_b$};
    \node[state] (c) [right = 3cm of b] {$q_c$};
    
    \draw (a) edge[bend left] node{\begin{tabular}{c}
                                        $!a$ \\ \hline
                                        \lstinline[language=C++]{a = true; x = 1;} \\
                                    \end{tabular}} (b);
    \draw (a) edge[bend right] node[below]{\begin{tabular}{c}
                                        $!a$ \\ \hline
                                        \lstinline[language=C++]{a = true; x = 2;} \\
                                    \end{tabular}} (b);
    \draw (b) edge node[above]{\lstinline{a = false;}} (c);
    \draw (a) edge[loop above] (a);
\end{tikzpicture}

Please note that this attemt is wrong and that this machine loops. This is because a new ``thread'' is spanwed every time the condition is true. To change this behavior; a new transition must be added.

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm, semithick]
    \node[initial, state] (a) {$q_a$};
    \node[state] (b) [right = 3cm of a] {$q_b$};
    \node[state] (c) [right = 3cm of b] {$q_c$};
    
    \draw (a) edge[bend left] node{\begin{tabular}{c}
                                        $!a$ \\ \hline
                                        \lstinline[language=C++]{a = true; x = 1;} \\
                                    \end{tabular}} (b);
    \draw (a) edge[bend right] node[below]{\begin{tabular}{c}
                                        $!a$ \\ \hline
                                        \lstinline[language=C++]{a = true; x = 2;} \\
                                    \end{tabular}} (b);
    \draw (b) edge node[above]{\lstinline{a = false;}} (c);
    \draw (a) edge[loop above] node[above]{\begin{tabular}{c}
                                        $a$ \\ \hline
                                    \end{tabular}} (a);
\end{tikzpicture}

Unfortunately this will increase the runtime of the execution on a computer to a higher order. This is undesired and to prevent that the transitions from a state are only considered once. This means, however, that the previous machine will not work. This does not mean that the machine cannot work. We must just make sure that a single ``thread'' doesn't check both transitions.

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm, semithick]
    \node[initial, state] (a) {$q_a$};
    \node[state] (b) [above right = 3cm of a] {$q_b$};
    \node[state] (c) [below right = 3cm of a] {$q_c$};
    \node[state] (d) [below right = 3cm of b] {$q_d$};
    \node[state] (e) [right = 3cm of d] {$q_e$};
    
    \draw (a) edge (b);
    \draw (a) edge (c);
    \draw (b) edge node{\begin{tabular}{c}
                                        $!a$ \\ \hline
                                        \lstinline[language=C++]{a = true; x = 1;} \\
                                    \end{tabular}} (d);
    \draw (c) edge node[below right]{\begin{tabular}{c}
                                        $!a$ \\ \hline
                                        \lstinline[language=C++]{a = true; x = 2;} \\
                                    \end{tabular}} (d);
    \draw (d) edge node[above]{\lstinline{a = false;}} (e);
    \draw (b) edge[loop above] node[above]{\begin{tabular}{c}
                                        $a$ \\ \hline
                                    \end{tabular}} (b);
    \draw (c) edge[loop below] node[below]{\begin{tabular}{c}
                                        $a$ \\ \hline
                                    \end{tabular}} (c);
\end{tikzpicture}

\section{IO}
[This section would contain the concept of IO]
