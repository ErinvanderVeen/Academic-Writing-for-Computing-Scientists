\section{States and Transitions}
\subsection{States}
States were defined as: ``a set of bits called state variables that contain all the information about the past necessary to explain the future behavior of the circuit.'' by D.M. Harris and S.L. Harris\footnote{D.M. Harris; S.L. Harris, Digital Design and Computer Architecture, 2007}. On a more higher level, we could say that the state of a program consists of the values of its variables, the location in the program, and the return address. The first is something we are all familiar with. From now on, I will refer to it as the ``variable state'' or $\mathcal{V}$. One could compare this ``variable state'' to the values on a the tape in a Turing machine. The second is typically implicit and will be named the ``location state'' or $\mathcal{S}$. The last will be disregarded since it is not of any importance for the type of language discussed in this paper. Elements of $\mathcal{V}$ will be defined as $[v_1 = x_1, v_2 = x_2, \dots, v_i = x_i]$ where $v_i$ is some variable and $x_i$ is the value of said variable in the state. $\mathcal{S}$ has the same definition as it has with Turing machines.

Consider the following (C++) program:
\begin{lstlisting}[language=C++]
    int main()
    {
        int x = 2;
        int y = 3;

        x = x * y;

        return 0;
    }
\end{lstlisting}

On line 2, the variable state is empty. Which I will denote as: $v_2 = [\ ]$. After we Assign $x$ and $y$, however, the state is no longer empty. A non-empty state, in this case $v_5$ will be denoted as: $[x=2, y=3]$. Now that this has been discussed, I am sure that finding the state after line 6 is easy. Namely: $[x=6, y=3]$.

\subsection{Transitions}
In the previous example there was an obvious change of state between $v_5$ and $v_7$. This change of state can be modeled in a ``Transition function''. These functions $\mathcal{F}$ take two inputs: $\mathcal{S}$ and $\mathcal{V}$.  The output is one or more variable states and one or more location states. I will explain why we can also have multiple states later. %TODO: Check if I actually explain it
Such a function can be defined as follows: $f: \mathcal{S} \times \mathcal{V} \mapsto \mathcal{S} \times \mathcal{V}$. In the above example there is just such a function. It takes the state $s$, and variable state $v$ with $s$ being the current line in the program and $v$ being $v_5$. The output $(s', v')$ is just as predictable with $s'$ pointing to line 7 and $v'$ as $v_7$. The notation of such a function is the standard C notation.

Putting these things together a machine can be constructed that is identical to this exact program.

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm, semithick]
    \node[initial, state]    (A)                     {$q_a$};
    \node[state]            (B) [right = 4cm of A]  {$q_b$};
    \node[state]            (C) [right of = B]      {$q_c$};
    
    \draw (A) edge node{int x = 2; int y = 3} (B);
    \draw (B) edge node{x = x * y} (C);
\end{tikzpicture}

This machine shows that the second input variable of the transition function is indeed implicit. It also shows the general idea behind the transitions; they contain, or carry, the statements. Every execution of such a statement, or combination of statements, brings us to another state. From there a next transition must be chosen. Choosing such a transition seems pointless when such a choice is purely random. For that reason a condition must be added to the transition function. I will define the conditions $\mathcal{C}$ as a 3-tuple: (a number or variable, a relation, another number of variable). The relation is defined as any of the standard boolean logic relations (e.g. $=, <, >, \leq, \geq, \rightarrow, \leftrightarrow, \dots$). The ``number or variable'' is either a global variable or a constant. A relation can not be tested on two different datatypes. The notation will be the following.

\begin{tabular}{c}
    $condition$ \\ \hline
    $function$
\end{tabular}

This new construction can be used to calculate the absolute value of a given number. If the given number is lower than 0; the number can be multiplied with -1. If the number is higher or equal to 0; the number need not be transformed. Note that a different state need not strictly be entered. The given number can, in the function, be instantaneously transformed to the required number.

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm, semithick]
    \node[initial, state] (a) {$q_a$};
    
    \draw (a) edge[loop above] node{\begin{tabular}{c}
                                        $x \geq 0$ \\ \hline
                                    \end{tabular}} (a);
    \draw (a) edge[loop below] node{\begin{tabular}{c}
                                        $x \leq 0$ \\ \hline
                                        x = -1 * x
                                    \end{tabular}} (a);

\end{tikzpicture}

The previous example shows that such machines can loop. As soon as the x is positive or 0, the program will do nothing but loop. This happens because the condition of the following transition is always true.

\begin{tabular}{c}
    $x \geq 0$ \\ \hline
\end{tabular}

Such a problem can be solved by adding an additional state.

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm, semithick]
    \node[initial, state] (a) {$q_a$};
    \node[state] (b) [right = 4cm of a] {$q_b$};
    
    \draw (a) edge[bend left] node{\begin{tabular}{c}
                                        $x \geq 0$ \\ \hline
                                    \end{tabular}} (b);
    \draw (a) edge[bend right] node{\begin{tabular}{c}
                                        $x \leq 0$ \\ \hline
                                        x = -1 * x
                                    \end{tabular}} (b);
\end{tikzpicture}

This program will terminate as soon as none of the conditions of the outgoing transitions are met. There is another way for such a program to terminate; final states. If every element of the ``location state'' is currently in a final state, the program will terminate.
